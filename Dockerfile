ARG NODE_VERSION

FROM node:${NODE_VERSION}-alpine

RUN yarn global add serve

WORKDIR /app
COPY package.json package.json
COPY yarn.lock yarn.lock

RUN yarn install

COPY src src
COPY public public
COPY tsconfig.json tsconfig.json

RUN yarn run build

CMD ["serve", "build"]
EXPOSE 3000
