import { Outlet, useRoutes } from "react-router-dom";
import { RouteObject } from "react-router/dist/lib/context";
import { MainLayout } from "components/Layout";
import { lazyImport } from "utilities/lazyImport";
import { Suspense } from "react";
import { Box, CircularProgress } from "@mui/material";

const { Dashboard } = lazyImport(
  () => import("../pages/Dashboard"),
  "Dashboard"
);

const ROUTES: RouteObject[] = [
  {
    path: "/",
    element: (
      <MainLayout>
        <Suspense
          fallback={
            <Box width="100%" display="flex">
              <CircularProgress sx={{ margin: "auto" }} size="6rem" />
            </Box>
          }
        >
          <Outlet />
        </Suspense>
      </MainLayout>
    ),
    children: [
      {
        path: "/",
        element: <Dashboard />,
      },
      {
        path: "/dashboard",
        element: <Dashboard />,
      },
    ],
  },
];

export const AppRoutes = () => {
  return useRoutes(ROUTES);
};
