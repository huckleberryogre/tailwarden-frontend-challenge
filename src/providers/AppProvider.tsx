import { PropsWithChildren } from "react";
import { QueryClientProvider } from "@tanstack/react-query";
import { queryClient } from "lib/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import CssBaseline from "@mui/material/CssBaseline";
import { store as appStore } from "store";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { AppThemeProvider } from "theme";

interface IProps extends PropsWithChildren {
  store?: typeof appStore;
}

export const AppProvider = ({ children, store = appStore }: IProps) => {
  return (
    <Provider store={store}>
      <AppThemeProvider>
        <QueryClientProvider client={queryClient}>
          <CssBaseline />
          <BrowserRouter>{children}</BrowserRouter>
          <ReactQueryDevtools />
        </QueryClientProvider>
      </AppThemeProvider>
    </Provider>
  );
};
