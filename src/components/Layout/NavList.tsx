import { useNavigate } from "react-router-dom";
import {
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Toolbar,
} from "@mui/material";
import { Dashboard as DashboardIcon } from "@mui/icons-material";

const ROUTES = [
  {
    to: "/dashboard",
    icon: <DashboardIcon />,
    text: "Dashboard",
  },
];

export const NavList = () => {
  const navigate = useNavigate();
  const handleClickListItem = (to: string) => () => {
    navigate(to);
  };

  return (
    <>
      <Toolbar />
      <Divider />
      <List>
        {ROUTES.map(({ to, icon, text }) => (
          <ListItem key={text} disablePadding>
            <ListItemButton onClick={handleClickListItem(to)}>
              <ListItemIcon>{icon}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </>
  );
};
