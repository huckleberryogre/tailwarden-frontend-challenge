import { IconButton, Toolbar, Typography, useTheme } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import LightModeIcon from "@mui/icons-material/LightMode";
import { MainAppBarStyled } from "./styled";
import { themeActions } from "store/slice/theme";
import { useDispatch } from "react-redux";

interface IProps {
  onDrawerToggle: () => void;
}

export const AppBar = ({ onDrawerToggle }: IProps) => {
  const theme = useTheme();
  const dispatch = useDispatch();

  return (
    <MainAppBarStyled>
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={onDrawerToggle}
          sx={{ mr: 2, display: { sm: "none" } }}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
          Tailwarden cloud app
        </Typography>
        <IconButton
          onClick={() => dispatch(themeActions.toggleTheme())}
          color="inherit"
        >
          {theme.palette.mode === "dark" ? <DarkModeIcon /> : <LightModeIcon />}
        </IconButton>
      </Toolbar>
    </MainAppBarStyled>
  );
};
