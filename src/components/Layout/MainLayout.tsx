import { Box, Toolbar } from "@mui/material";
import { PropsWithChildren, useState } from "react";
import { Drawer } from "./Drawer";
import { AppBar } from "./AppBar";
import { LayoutPaperStyled, MainBoxStyled } from "./styled";

export const MainLayout = ({ children }: PropsWithChildren) => {
  const [mobileOpen, setMobileOpen] = useState(false);
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <Box height="100vh">
      <AppBar onDrawerToggle={handleDrawerToggle} />
      <Drawer mobileOpen={mobileOpen} onDrawerToggle={handleDrawerToggle} />
      <MainBoxStyled component="main">
        <Toolbar />
        <LayoutPaperStyled>{children}</LayoutPaperStyled>
      </MainBoxStyled>
    </Box>
  );
};
