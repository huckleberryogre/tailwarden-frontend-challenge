import {
  DrawerBoxStyled,
  DrawerDesktopStyled,
  DrawerMobileStyled,
} from "./styled";
import { NavList } from "./NavList";

interface IProps {
  mobileOpen: boolean;
  onDrawerToggle: () => void;
}

export const Drawer = ({ mobileOpen, onDrawerToggle }: IProps) => {
  return (
    <DrawerBoxStyled component="nav" aria-label="mailbox folders">
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <DrawerMobileStyled
        variant="temporary"
        open={mobileOpen}
        onClose={onDrawerToggle}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
      >
        <NavList />
      </DrawerMobileStyled>
      <DrawerDesktopStyled variant="permanent" open>
        <NavList />
      </DrawerDesktopStyled>
    </DrawerBoxStyled>
  );
};
