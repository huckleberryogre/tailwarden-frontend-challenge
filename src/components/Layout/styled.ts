import { AppBar, Box, Drawer, Paper, styled } from "@mui/material";
import { DRAWER_WIDTH } from "./constants";

export const LayoutPaperStyled = styled(Paper)(({ theme }) => ({
  minHeight: "calc(100vh - 64px)",
  display: "flex",
  padding: theme.spacing(2),
  width: "100%",
}));

export const MainBoxStyled = styled(Box)(({ theme }) => ({
  flexGrow: 1,
  [theme.breakpoints.up("sm")]: {
    width: `calc(100% - ${DRAWER_WIDTH}px)`,
    marginLeft: `${DRAWER_WIDTH}px`,
  },
}));

export const MainAppBarStyled = styled(AppBar)(({ theme }) => ({
  position: "fixed",
  [theme.breakpoints.up("sm")]: {
    ml: `${DRAWER_WIDTH}px`,
    width: `calc(100% - ${DRAWER_WIDTH}px)`,
  },
}));

export const DrawerBoxStyled = styled(Box)(({ theme }) => ({
  [theme.breakpoints.up("sm")]: {
    width: `${DRAWER_WIDTH}px`,
    flexShrink: 0,
  },
}));

export const DrawerMobileStyled = styled(Drawer)(({ theme }) => ({
  [theme.breakpoints.up("sm")]: {
    display: "none",
  },
  [theme.breakpoints.down("sm")]: {
    display: "block",
  },
  "& .MuiDrawer-paper": {
    boxSizing: "border-box",
    width: DRAWER_WIDTH,
  },
}));

export const DrawerDesktopStyled = styled(Drawer)(({ theme }) => ({
  [theme.breakpoints.up("sm")]: {
    display: "block",
  },
  [theme.breakpoints.down("sm")]: {
    display: "none",
  },
  "& .MuiDrawer-paper": {
    boxSizing: "border-box",
    width: DRAWER_WIDTH,
  },
}));
