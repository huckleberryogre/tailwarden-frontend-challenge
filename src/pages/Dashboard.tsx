import { AccountCosts, AccountSelect, AccountStats } from "modules";
import { useSelector } from "react-redux";
import { selectAccount } from "store/slice/accounts";
import { Box } from "@mui/material";

/**
 * Dashboard page
 */
export const Dashboard = () => {
  const account = useSelector(selectAccount);

  return (
    <Box aria-label="dashboard-page" sx={{ width: "100%" }}>
      <AccountSelect />
      {account && <AccountStats accountId={account.id} />}
      {account && <AccountCosts accountId={account.id} />}
    </Box>
  );
};
