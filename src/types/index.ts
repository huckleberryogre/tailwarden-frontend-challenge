import { EAccountCostsGroupKey, EAccountStats } from "enums";

/**
 * Cloud account model.
 */
export interface ICloudAccount {
  id: string;
  provider: string;
  label: string;
  logo: string;
}

/**
 * Cloud account stats model.
 */
export type TCloudAccountStats = {
  [key in EAccountStats]: number;
};

/**
 * Cloud account costs group model.
 */
export interface ICloudAccountCostsGroup {
  key: EAccountCostsGroupKey;
  amount: number;
}

/**
 * Cloud account costs model.
 */
export interface ICloudAccountCosts {
  date: string;
  groups: ICloudAccountCostsGroup[];
}
