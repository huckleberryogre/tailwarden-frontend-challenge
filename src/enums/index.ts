/**
 * Account costs group key.
 */
export enum EAccountCostsGroupKey {
  Compute = "Compute",
  Storage = "Storage",
  Network = "Network",
}

/**
 * Account stats key.
 */
export enum EAccountStats {
  bill = "bill",
  servers = "servers",
  regions = "regions",
  alarms = "alarms",
}

/**
 * App theme.
 */
export enum EAppTheme {
  light = "light",
  dark = "dark",
}
