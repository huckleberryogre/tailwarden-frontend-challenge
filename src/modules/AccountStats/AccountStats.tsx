import { useAccountStats } from "api/accounts";
import { AccountStatsCard } from "./AccountStatsCard";
import { GridStyled } from "modules/AccountStats/styled";
import { EAccountStats } from "enums";

interface IProps {
  accountId: string;
}

/**
 * Account stats module
 */
export const AccountStats = ({ accountId }: IProps) => {
  const { data, isFetching } = useAccountStats(accountId);

  return (
    <div aria-label="account-stats-module">
      <GridStyled my={1} container>
        {Object.values<EAccountStats>(EAccountStats).map((key) => (
          <AccountStatsCard
            key={key}
            statsKey={key}
            statsValue={data && data[key]}
            isFetching={isFetching}
          />
        ))}
      </GridStyled>
    </div>
  );
};
