import {
  Card,
  CardContent,
  CardMedia,
  Grid,
  styled,
  SvgIcon,
} from "@mui/material";

export const CardStyled = styled(Card)(() => ({
  display: "flex",
  width: 220,
  height: 80,
}));

export const CardContentStyled = styled(CardContent)(() => ({
  padding: "5px 16px",
  paddingBottom: "5px !important",
  width: "140px",
}));

export const CardMediaStyled = styled(CardMedia)(() => ({
  display: "flex",
  alignItems: "center",
  width: 80,
}));

export const IconStyled = (Component: typeof SvgIcon) =>
  styled(Component)(() => ({
    width: "70px",
    height: "70px",
    margin: "auto",
  }));

export const GridStyled = styled(Grid)(() => ({
  maxWidth: "1000px",
}));
