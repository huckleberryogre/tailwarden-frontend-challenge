import { TCloudAccountStats } from "types";
import { SvgIcon } from "@mui/material";
import { EAccountStats } from "enums";
import PaidIcon from "@mui/icons-material/Paid";
import NotificationsIcon from "@mui/icons-material/Notifications";
import PublicIcon from "@mui/icons-material/Public";
import StorageIcon from "@mui/icons-material/Storage";

/**
 * Icons mapping for account stats.
 */
export const IconsMapping: {
  [key in keyof TCloudAccountStats]: typeof SvgIcon;
} = {
  [EAccountStats.bill]: PaidIcon,
  [EAccountStats.alarms]: NotificationsIcon,
  [EAccountStats.regions]: PublicIcon,
  [EAccountStats.servers]: StorageIcon,
};
