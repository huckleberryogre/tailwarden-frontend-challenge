import { Grid, Skeleton, Typography } from "@mui/material";
import { TCloudAccountStats } from "types";
import {
  CardContentStyled,
  CardMediaStyled,
  CardStyled,
  IconStyled,
} from "./styled";
import { EAccountStats } from "enums";
import { IconsMapping } from "modules/AccountStats/constants";

interface IProps {
  statsKey: keyof TCloudAccountStats;
  statsValue?: string | number;
  isFetching: boolean;
}

export const AccountStatsCard = ({
  statsKey,
  statsValue,
  isFetching,
}: IProps) => {
  const Icon = IconStyled(IconsMapping[statsKey]);

  return (
    <Grid key={statsKey} item xs={12} md={6} lg={3} my={1}>
      <CardStyled>
        <CardMediaStyled>
          <Icon color="secondary" />
        </CardMediaStyled>
        <CardContentStyled>
          <Typography
            textAlign="right"
            variant="h5"
            color="text.secondary"
            component="div"
          >
            {statsKey.charAt(0).toUpperCase() + statsKey.slice(1)}
          </Typography>
          {isFetching ? (
            <Skeleton variant="text" sx={{ fontSize: "1.5rem" }} />
          ) : (
            <Typography
              textAlign="right"
              component="div"
              variant="h5"
              sx={{ width: "100%" }}
            >
              {`${statsKey === EAccountStats.bill ? "$" : ""}${statsValue}`}
            </Typography>
          )}
        </CardContentStyled>
      </CardStyled>
    </Grid>
  );
};
