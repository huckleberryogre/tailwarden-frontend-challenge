import {
  Avatar,
  Box,
  FormControl,
  InputLabel,
  ListItemIcon,
  ListItemText,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { useAccounts } from "api/accounts";
import { accountsActions, selectAccount } from "store/slice/accounts";
import { useDispatch, useSelector } from "react-redux";
import { texts } from "./texts";

/**
 * Account select module
 */
export const AccountSelect = () => {
  const dispatch = useDispatch();
  const selectedAccount = useSelector(selectAccount);
  const { data, isFetching } = useAccounts();
  const hasAccounts = !!data?.length;
  const label = isFetching
    ? texts.selectLabel.loading
    : hasAccounts
    ? texts.selectLabel.default
    : texts.selectLabel.noOptions;

  const options = data?.map((account) => (
    <MenuItem role="option" key={account.id} value={account.id}>
      <Box display="flex" alignItems="center">
        <ListItemIcon sx={{ minWidth: 32 }}>
          <Avatar
            alt={account.provider}
            src={account.logo}
            sx={{ width: 24, height: 24 }}
          />
        </ListItemIcon>
        <ListItemText>{`${account.provider} (${account.label})`}</ListItemText>
      </Box>
    </MenuItem>
  ));

  const handleChange = (event: SelectChangeEvent) => {
    const account = data?.find((account) => account.id === event.target.value);

    if (account) {
      dispatch(accountsActions.select(account));
    }
  };

  return (
    <div aria-label="account-select-module">
      <FormControl sx={{ minWidth: 200 }}>
        <InputLabel id="account-select-label">{label}</InputLabel>
        <Select
          value={selectedAccount?.id || ""}
          label={label}
          labelId={"account-select-label"}
          onChange={handleChange}
          disabled={isFetching || !hasAccounts}
        >
          {options}
        </Select>
      </FormControl>
    </div>
  );
};
