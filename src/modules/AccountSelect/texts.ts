export const texts = {
  selectLabel: {
    loading: "Loading accounts...",
    noOptions: "No options",
    default: "Select account",
  },
};
