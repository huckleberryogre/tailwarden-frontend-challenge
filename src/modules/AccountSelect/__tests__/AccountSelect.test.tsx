import { fireEvent, screen, waitFor } from "@testing-library/react";
import { AccountSelect } from "modules";
import { texts } from "../texts";
import { server } from "__mocks__/server";
import { rest } from "msw";
import { accountsApiUrls } from "api/accounts";
import { ICloudAccount } from "types";
import { renderWithProviders } from "utilities/renderWithProviders";
import { accountsMock } from "__mocks__/api/accounts.response";

const { label, provider } = accountsMock[0];
const labelText = `${provider} (${label})`;

const mockGetAccounts = (response: ICloudAccount[]) => {
  server.use(
    rest.get(accountsApiUrls.accounts, (req, res, ctx) =>
      res(ctx.status(200), ctx.json(response))
    )
  );
};

describe("AccountSelect", () => {
  describe('when I render the "AccountSelect" component', () => {
    it("renders", async () => {
      const { container } = renderWithProviders(<AccountSelect />);

      expect(container).toHaveTextContent(texts.selectLabel.loading);

      await waitFor(() => {
        expect(container).toHaveTextContent(texts.selectLabel.default);
      });
    });
  });

  describe("when I select an account", () => {
    it("selects the account", async () => {
      renderWithProviders(<AccountSelect />);

      await screen.findByLabelText(texts.selectLabel.default);
      const selectButton = await screen.findByRole("button");
      fireEvent.mouseDown(selectButton);

      const option = await screen.findByRole("option");
      expect(option.textContent).toBe(labelText);
      fireEvent.click(option);

      expect(screen.getByRole("button").textContent).toBe(labelText);
    });
  });

  describe("when there are no accounts", () => {
    it("renders disabled state with no-options label", async () => {
      mockGetAccounts([]);
      const { container } = renderWithProviders(<AccountSelect />);

      expect(container).toHaveTextContent(texts.selectLabel.loading);
      await waitFor(() =>
        expect(container).toHaveTextContent(texts.selectLabel.noOptions)
      );
    });
  });
});
