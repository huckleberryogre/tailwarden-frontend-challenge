import { useAccountCosts } from "api/accounts";
import { Grid } from "@mui/material";
import { Bar } from "./charts/Bar";
import { Pie } from "./charts/Pie";

interface IProps {
  accountId: string;
}

/**
 * Account costs module
 */
export const AccountCosts = ({ accountId }: IProps) => {
  const { data, status, isFetching } = useAccountCosts(accountId);

  return (
    <div aria-label="account-costs-module">
      <Grid container my={2}>
        <Grid item sm={12} md={6} sx={{ width: "100%" }} my={2}>
          <Bar data={data} status={status} isFetching={isFetching}></Bar>
        </Grid>
        <Grid item sm={12} md={6} sx={{ width: "100%" }} my={2}>
          <Pie data={data} status={status} isFetching={isFetching}></Pie>
        </Grid>
      </Grid>
    </div>
  );
};
