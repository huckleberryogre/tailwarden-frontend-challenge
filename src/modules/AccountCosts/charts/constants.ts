import { ChartOptions } from "chart.js/dist/types";
import { texts } from "../texts";
import { EClientSize } from "utilities/useClientSize";
import { EAccountCostsGroupKey } from "enums";

/**
 * Bar chart options getter.
 *
 * @param monthsCount Number of months to display.
 * @param size Client size.
 */
export const getBarChartOptions = (
  monthsCount: number,
  size: EClientSize
): ChartOptions<"bar"> => {
  const defaultOptions: ChartOptions<"bar"> = {
    plugins: {
      title: {
        display: true,
        text: texts.barChart.title(monthsCount),
      },
    },
    responsive: true,
    interaction: {
      mode: "index",
      intersect: false,
    },
    maintainAspectRatio: false,
  };

  const smallScreenOptions: ChartOptions<"bar"> = {
    scales: {
      x: {
        stacked: true,
        ticks: {
          autoSkip: true,
          maxTicksLimit: 5,
        },
      },
      y: {
        ticks: {
          autoSkip: true,
          maxTicksLimit: 5,
        },
      },
    },
  };

  return size === EClientSize.sm
    ? {
        ...defaultOptions,
        ...smallScreenOptions,
      }
    : defaultOptions;
};

/**
 * Pie chart options.
 */
export const pieChartOptions: ChartOptions<"pie"> = {
  plugins: {
    title: {
      display: true,
      text: texts.pieChart.title,
    },
  },
  responsive: true,
  maintainAspectRatio: false,
};

/**
 * Helper type for color mapping.
 */
export type TColorMapping = {
  [key in EAccountCostsGroupKey]: string;
};

/**
 * Color mapping for account costs charts.
 * @param transparency Transparency value for rgba color.
 */
export const getColorMapping = (transparency = 0.75): TColorMapping => ({
  Compute: `rgba(54, 162, 235, ${transparency})`,
  Storage: `rgba(255, 206, 86, ${transparency})`,
  Network: `rgba(175, 92, 92, ${transparency})`,
});

/**
 * Chart height mapping for different client sizes.
 */
export const chartHeightMapping = {
  [EClientSize.sm]: 250,
  [EClientSize.md]: 300,
  [EClientSize.lg]: 350,
  [EClientSize.xl]: 400,
};

/**
 * Pie chart drawing area percent mapping for better skeleton feedback.
 */
export const pieChartDrawingAreaPercentMapping = {
  [EClientSize.sm]: 0.65,
  [EClientSize.md]: 0.7,
  [EClientSize.lg]: 0.8,
  [EClientSize.xl]: 0.85,
};
