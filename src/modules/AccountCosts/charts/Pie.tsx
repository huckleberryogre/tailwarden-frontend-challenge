import { ICloudAccountCosts } from "types";
import { ArcElement, Chart as ChartJS, Legend, Tooltip } from "chart.js";
import { Box, Skeleton } from "@mui/material";
import { Pie as PieChart } from "react-chartjs-2";
import { useClientSize } from "utilities/useClientSize";
import { getPieChartDataMapping } from "../utilities/getPieChartDataMapping";
import {
  chartHeightMapping,
  pieChartDrawingAreaPercentMapping,
  pieChartOptions,
} from "./constants";

ChartJS.register(ArcElement, Tooltip, Legend);

interface IProps {
  data?: ICloudAccountCosts[];
  status: "success" | "error" | "loading";
  isFetching: boolean;
}

export const Pie = ({ data, status, isFetching }: IProps) => {
  const clientSize = useClientSize();
  const pieData = getPieChartDataMapping(data);
  const height = chartHeightMapping[clientSize];
  const chartAreaHeight =
    height * pieChartDrawingAreaPercentMapping[clientSize];
  const showChart = !isFetching && status === "success" && data && pieData;
  const padding = height - chartAreaHeight;

  return (
    <Box
      aria-label="pie-chart"
      p={isFetching ? `${padding}px 20px` : undefined}
      sx={{ height: height, display: "flex", justifyContent: "center" }}
    >
      {isFetching && chartAreaHeight && (
        <Skeleton
          variant="circular"
          width={chartAreaHeight}
          height={chartAreaHeight}
        />
      )}
      {showChart && (
        <PieChart
          data={pieData}
          options={pieChartOptions}
          width={height}
          height={height}
          redraw
        />
      )}
    </Box>
  );
};
