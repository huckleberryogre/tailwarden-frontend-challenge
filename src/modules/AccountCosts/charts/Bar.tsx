import { Bar as BarChart } from "react-chartjs-2";
import { Box, Skeleton } from "@mui/material";
import {
  BarElement,
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LinearScale,
  Title,
  Tooltip,
} from "chart.js";
import { getBarChartDataMapping } from "modules/AccountCosts/utilities/getBarChartDataMapping";
import {
  chartHeightMapping,
  getBarChartOptions,
} from "modules/AccountCosts/charts/constants";
import { ICloudAccountCosts } from "types";
import { useClientSize } from "utilities/useClientSize";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

interface IProps {
  data?: ICloudAccountCosts[];
  status: "success" | "error" | "loading";
  isFetching: boolean;
}

export const Bar = ({ data, status, isFetching }: IProps) => {
  const barData = getBarChartDataMapping(data);
  const clientSize = useClientSize();
  const height = chartHeightMapping[clientSize];
  const showChart = !isFetching && status === "success" && data && barData;
  const paddingTop = 60;
  const paddingBottom = 0;

  return (
    <Box
      aria-label="bar-chart"
      p={isFetching ? `${paddingTop}px 20px ${paddingBottom}px` : undefined}
      sx={{ height: height }}
    >
      {isFetching && (
        <Skeleton
          variant="rounded"
          width="100%"
          height={height - paddingTop - paddingBottom}
        />
      )}
      {showChart && (
        <BarChart
          data={barData}
          options={getBarChartOptions(barData.labels.length, clientSize)}
          width="100%"
          height={height}
          redraw
        />
      )}
    </Box>
  );
};
