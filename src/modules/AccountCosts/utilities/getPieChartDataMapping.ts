import { ICloudAccountCosts } from "types";
import { EAccountCostsGroupKey } from "enums";
import { getColorMapping } from "../charts/constants";
import { ChartData } from "chart.js";

type TMappedData = {
  [key in EAccountCostsGroupKey]: number;
};

export const getPieChartDataMapping = (
  data?: ICloudAccountCosts[]
): Maybe<ChartData<"pie">> => {
  let pieData;

  if (data) {
    const mappedData = data.reduce<TMappedData>((acc, { groups }) => {
      groups.forEach(({ key, amount }) => {
        if (acc[key]) {
          acc[key] += amount;
        } else {
          acc[key] = amount;
        }
      });
      return acc;
    }, {} as TMappedData);

    const colors = (transparency: number) =>
      Object.keys(mappedData).map(
        (key) => getColorMapping(transparency)[key as EAccountCostsGroupKey]
      );

    const datasets = [
      {
        data: Object.values(mappedData),
        backgroundColor: colors(0.3),
        borderColor: colors(1),
        borderWidth: 1,
      },
    ];

    pieData = {
      labels: Object.keys(mappedData),
      datasets,
    };
  }

  return pieData;
};
