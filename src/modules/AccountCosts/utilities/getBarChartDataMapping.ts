import { ICloudAccountCosts } from "types";
import { getMonthFromDateString } from "utilities/getMonthFromDateString";
import { getColorMapping } from "../charts/constants";
import { EAccountCostsGroupKey } from "enums";

type TMappedData = {
  [key in EAccountCostsGroupKey]: number[];
};

/**
 * Maps the server data to a format that can be used by the bar chart.
 *
 * @param data The server data.
 */
export const getBarChartDataMapping = (data?: ICloudAccountCosts[]) => {
  let barData;

  if (data) {
    const mappedData = data.reduce<TMappedData>((acc, { groups }) => {
      groups.forEach(({ key, amount }) => {
        if (acc[key]) {
          acc[key].push(amount);
        } else {
          acc[key] = [amount];
        }
      });
      return acc;
    }, {} as TMappedData);

    const datasets = Object.entries(mappedData).map(([key, data]) => ({
      label: key,
      data,
      backgroundColor: getColorMapping(0.3)[key as EAccountCostsGroupKey],
      borderColor: getColorMapping(1)[key as EAccountCostsGroupKey],
      borderWidth: 1,
    }));

    barData = {
      labels: data.map(({ date }) => getMonthFromDateString(date)),
      datasets,
    };
  }

  return barData;
};
