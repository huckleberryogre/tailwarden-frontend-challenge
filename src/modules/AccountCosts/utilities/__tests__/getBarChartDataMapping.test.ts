import { getCloudAccountCostsMock } from "__mocks__";
import { getBarChartDataMapping } from "modules/AccountCosts/utilities/getBarChartDataMapping";
import { getMonthFromDateString } from "utilities/getMonthFromDateString";
import { EAccountCostsGroupKey } from "enums";
import { getColorMapping } from "modules/AccountCosts/charts/constants";

describe("getBarChartDataMapping", () => {
  describe('when "data" is correct', () => {
    it("returns the correct data mapping", () => {
      const data = getCloudAccountCostsMock(2);
      const [
        {
          date: date1,
          groups: [
            { amount: amount1 },
            { amount: amount2 },
            { amount: amount3 },
          ],
        },
        {
          date: date2,
          groups: [
            { amount: amount4 },
            { amount: amount5 },
            { amount: amount6 },
          ],
        },
      ] = data;
      const result = getBarChartDataMapping(data);

      expect(result).toEqual({
        labels: [getMonthFromDateString(date1), getMonthFromDateString(date2)],
        datasets: [
          {
            label: EAccountCostsGroupKey.Compute,
            data: [amount1, amount4],
            backgroundColor:
              getColorMapping(0.3)[EAccountCostsGroupKey.Compute],
            borderColor: getColorMapping(1)[EAccountCostsGroupKey.Compute],
            borderWidth: 1,
          },
          {
            label: EAccountCostsGroupKey.Network,
            data: [amount2, amount5],
            backgroundColor:
              getColorMapping(0.3)[EAccountCostsGroupKey.Network],
            borderColor: getColorMapping(1)[EAccountCostsGroupKey.Network],
            borderWidth: 1,
          },
          {
            label: EAccountCostsGroupKey.Storage,
            data: [amount3, amount6],
            backgroundColor:
              getColorMapping(0.3)[EAccountCostsGroupKey.Storage],
            borderColor: getColorMapping(1)[EAccountCostsGroupKey.Storage],
            borderWidth: 1,
          },
        ],
      });
    });
  });

  describe('when "data" is undefined', () => {
    it("returns undefined", () => {
      const result = getBarChartDataMapping(undefined);
      expect(result).toBeUndefined();
    });
  });
});
