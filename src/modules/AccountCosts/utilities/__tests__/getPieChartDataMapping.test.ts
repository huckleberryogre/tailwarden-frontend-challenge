import { getCloudAccountCostsMock } from "__mocks__";
import { getPieChartDataMapping } from "modules/AccountCosts/utilities/getPieChartDataMapping";
import { EAccountCostsGroupKey } from "enums";
import { getColorMapping } from "modules/AccountCosts/charts/constants";

describe("getPieChartDataMapping", () => {
  describe('when "data" is correct', () => {
    it("returns the correct data mapping", () => {
      const data = getCloudAccountCostsMock(2);
      const [
        {
          groups: [
            { amount: amount1 },
            { amount: amount2 },
            { amount: amount3 },
          ],
        },
        {
          groups: [
            { amount: amount4 },
            { amount: amount5 },
            { amount: amount6 },
          ],
        },
      ] = data;
      const result = getPieChartDataMapping(data);
      const labels = [
        EAccountCostsGroupKey.Compute,
        EAccountCostsGroupKey.Network,
        EAccountCostsGroupKey.Storage,
      ];
      const colors = (transparency: number) =>
        labels.map((key) => getColorMapping(transparency)[key]);

      expect(result).toEqual({
        labels: [
          EAccountCostsGroupKey.Compute,
          EAccountCostsGroupKey.Network,
          EAccountCostsGroupKey.Storage,
        ],
        datasets: [
          {
            data: [amount1 + amount4, amount2 + amount5, amount3 + amount6],
            backgroundColor: colors(0.3),
            borderColor: colors(1),
            borderWidth: 1,
          },
        ],
      });
    });
  });

  describe('when "data" is undefined', () => {
    it("returns undefined", () => {
      const result = getPieChartDataMapping(undefined);

      expect(result).toBeUndefined();
    });
  });
});
