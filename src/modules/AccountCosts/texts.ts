export const texts = {
  barChart: {
    title: (count: number) => `Last ${count} months spend by service`,
  },
  pieChart: {
    title: "Spend by service",
  },
};
