import { rest } from "msw";
import { accountsApiUrls } from "api/accounts";
import { accountsMock } from "__mocks__";

export const handlers = [
  rest.get(accountsApiUrls.accounts, (req, res, ctx) =>
    res(ctx.status(200), ctx.json(accountsMock))
  ),
];
