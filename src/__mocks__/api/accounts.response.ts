import { ICloudAccount, ICloudAccountCosts } from "types";
import { faker } from "@faker-js/faker";
import { EAccountCostsGroupKey } from "enums";

export const accountsMock: ICloudAccount[] = [
  { id: "1", provider: "provider1", label: "label1", logo: "logo1" },
];
export const emptyAccountsMock: ICloudAccount[] = [];

export const getCloudAccountCostsItemMock = (): ICloudAccountCosts => ({
  date: faker.date.past().toJSON(),
  groups: [
    {
      key: EAccountCostsGroupKey.Compute,
      amount: faker.datatype.number({ min: 0, max: 100 }),
    },
    {
      key: EAccountCostsGroupKey.Network,
      amount: faker.datatype.number({ min: 0, max: 100 }),
    },
    {
      key: EAccountCostsGroupKey.Storage,
      amount: faker.datatype.number({ min: 0, max: 100 }),
    },
  ],
});

export const getCloudAccountCostsMock = (length = 12): ICloudAccountCosts[] =>
  Array.from({ length }, getCloudAccountCostsItemMock);
