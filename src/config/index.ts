/**
 * Application configuration
 */
export const isDevelopment = process.env.NODE_ENV === "development";
export const API_URL = process.env.REACT_APP_API_URL as string;
