import { createTheme, ThemeProvider } from "@mui/material";
import { PropsWithChildren, useMemo } from "react";
import { useSelector } from "react-redux";
import { selectMode } from "store/slice/theme";

export const AppThemeProvider = ({ children }: PropsWithChildren) => {
  const mode = useSelector(selectMode);
  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode,
        },
      }),
    [mode]
  );

  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
