import axios from "axios";
import { ICloudAccount, ICloudAccountCosts, TCloudAccountStats } from "types";
import { useQuery } from "@tanstack/react-query";

export const accountsApiUrls = {
  accounts: "https://hiring.tailwarden.com/v1/accounts",
  accountStats: (id: string) =>
    `https://hiring.tailwarden.com/v1/accounts/${id}`,
  accountCosts: (id: string) =>
    `https://hiring.tailwarden.com/v1/accounts/${id}/history`,
};

export const accountsAPI = {
  get: {
    accounts: () => axios.get<ICloudAccount[]>(accountsApiUrls.accounts),
    accountStats: (id: string) =>
      axios.get<TCloudAccountStats>(accountsApiUrls.accountStats(id)),
    accountCosts: (id: string) =>
      axios.get<ICloudAccountCosts[]>(accountsApiUrls.accountCosts(id)),
  },
};

export const useAccounts = () => {
  return useQuery<ICloudAccount[]>(["accounts"], () =>
    accountsAPI.get.accounts().then((res) => res.data)
  );
};

export const useAccountStats = (id: string) => {
  return useQuery<TCloudAccountStats>(["accountStats", id], () =>
    accountsAPI.get.accountStats(id).then((res) => res.data)
  );
};

export const useAccountCosts = (id: string) => {
  return useQuery<ICloudAccountCosts[]>(["accountCosts", id], () =>
    accountsAPI.get.accountCosts(id).then((res) => res.data)
  );
};
