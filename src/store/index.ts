import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import { accountsSlice } from "./slice/accounts";
import { themeSlice } from "./slice/theme";

export const getStore = () =>
  configureStore({
    reducer: {
      accounts: accountsSlice.reducer,
      theme: themeSlice.reducer,
    },
  });

export const store = getStore();

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
