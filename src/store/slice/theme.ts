import { EAppTheme } from "enums";
import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "store/index";

interface IThemeState {
  mode: EAppTheme.light | EAppTheme.dark;
}

const initialState: IThemeState = {
  mode: EAppTheme.light,
};

/**
 * Theme slice
 */
export const themeSlice = createSlice({
  name: "theme",
  initialState,
  reducers: {
    setTheme: (
      state,
      action: { payload: EAppTheme.light | EAppTheme.dark }
    ) => {
      state.mode = action.payload;
    },
    toggleTheme: (state) => {
      state.mode =
        state.mode === EAppTheme.light ? EAppTheme.dark : EAppTheme.light;
    },
  },
});

export const themeActions = themeSlice.actions;
export const selectMode = (state: RootState) => state.theme.mode;
