import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICloudAccount } from "types";
import { RootState } from "../index";

interface IAccountsState {
  selectedAccount: Nullable<ICloudAccount>;
}

const initialState: IAccountsState = {
  selectedAccount: null,
};

/**
 * Accounts slice
 */
export const accountsSlice = createSlice({
  name: "accounts",
  initialState,
  reducers: {
    select: (state, action: PayloadAction<ICloudAccount>) => {
      state.selectedAccount = action.payload;
    },
    clearSelected: (state) => {
      state.selectedAccount = null;
    },
  },
});

export const accountsActions = accountsSlice.actions;
export const selectAccount = (state: RootState) =>
  state.accounts.selectedAccount;
