import { getMonthFromDateString } from "utilities/getMonthFromDateString";

describe("getMonthFromDateString", () => {
  it("should return the month name from a date string", () => {
    expect(getMonthFromDateString("2020-01-01")).toBe("January");
    expect(getMonthFromDateString("2020-02-01")).toBe("February");
    expect(getMonthFromDateString("2020-12-01")).toBe("December");
    expect(getMonthFromDateString("2020-12-01")).toBe("December");
    expect(getMonthFromDateString("2020-01-01T00:00:00.000Z")).toBe("January");
    expect(getMonthFromDateString("2020-02-01T00:00:00.000Z")).toBe("February");
  });
  it("should return undefined if the date string is invalid", () => {
    expect(getMonthFromDateString("2020-13-01")).toBe("undefined");
    expect(getMonthFromDateString("2020-01-32")).toBe("undefined");
    expect(getMonthFromDateString("")).toBe("undefined");
  });
});
