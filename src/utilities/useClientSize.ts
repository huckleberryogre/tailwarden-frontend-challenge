import { useMediaQuery, useTheme } from "@mui/material";

export enum EClientSize {
  sm = "sm",
  md = "md",
  lg = "lg",
  xl = "xl",
}

export const useClientSize = () => {
  const theme = useTheme();
  const isSmall = useMediaQuery(theme.breakpoints.down("sm"));
  const isMedium = useMediaQuery(theme.breakpoints.down("md"));
  const isLarge = useMediaQuery(theme.breakpoints.down("lg"));

  if (isSmall) return EClientSize.sm;
  if (isMedium) return EClientSize.md;
  if (isLarge) return EClientSize.lg;
  return EClientSize.xl;
};
