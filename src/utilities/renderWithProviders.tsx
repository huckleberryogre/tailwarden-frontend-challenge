import { render, RenderOptions } from "@testing-library/react";
import { AppProvider } from "providers/AppProvider";
import { getStore } from "store";
import { ReactElement } from "react";

/**
 * Convenience function to render a component with basic providers and store
 *
 * @param ui Rendered component
 * @param [store] Redux store
 * @param [options] Render options
 */
export const renderWithProviders = (
  ui: ReactElement,
  store = getStore(),
  options: RenderOptions = {
    wrapper: ({ children }) => (
      <AppProvider store={store}>{children}</AppProvider>
    ),
  }
) => render(ui, options);
