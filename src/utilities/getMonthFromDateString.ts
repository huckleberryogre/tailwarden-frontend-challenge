const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
export const getMonthFromDateString = (dateString: string) => {
  try {
    const date = new Date(dateString);

    if (date.toString() === "Invalid Date") {
      throw new Error("Invalid date format provided: " + dateString);
    }

    return monthNames[date.getMonth()];
  } catch (error) {
    console.error(error);
    return "undefined";
  }
};
