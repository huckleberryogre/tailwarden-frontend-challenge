# Tailwarden frontend challenge

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) TS template.

### 🚀 Fast start with docker:

### `yarn run docker-build`

### `yarn run docker-start`

### 📌 Main commands for development:

### `yarn start`

### `yarn test`

### `yarn build`

### 👓 App features

- implemented ✨:

  - [x] basic app setup with CRA
  - [x] testing tools setup for comfortable testing + mocking server requests with MSW
  - [x] responsive app layout with Material UI as component library
  - [x] basic theme setup with support for dark mode
  - [x] routing with support for lazy imports
  - [x] api layer with axios + react query setup
  - [x] page/module/component structure
  - [x] client state management with redux + toolkit
  - [x] typescript global and shared types setup
  - [x] code quality tools: prettier, eslint + precommit hooks with husky
  - [x] basic utilities
  - [x] docker setup
  - [x] task given in the challenge within this app:
    - advanced loading state with skeleton component
    - responsive markup
    - charts with chart.js library
    - tests for components and utils

- planned 📈:
  - [ ] add more tests: current test coverage is about 50% which is not enough
  - [ ] review jest-fail-on-console package for the possibility of better testing
  - [ ] protected/public routes
  - [ ] add notifications (https://notistack.com/)
  - [ ] form validations (https://react-hook-form.com/)
  - [ ] e2e tests with Playwright/Cypress
  - [ ] error handling with error boundary
  - [ ] documentation system
